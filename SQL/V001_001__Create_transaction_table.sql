CREATE TABLE public.fact_transactions
(
 trans_id INT,
 create_date TIMESTAMP,
 modify_date TIMESTAMP,
 amount NUMERIC(18,4)
);
